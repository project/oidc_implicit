<?php
/**
 * @file
 * Administration form for the OpenID Connect Implicit Flow module.
 */

/**
 * Connection information configuration form callback.
 */
function oidc_implicit_admin_connection_form() {
  $connection_info = variable_get('oidc_implicit_connection_info', []);

  $form = [];

  $form['oidc_implicit_connection_info'] = [
    '#collapsed' => false,
    '#collapsible' => false,
    '#title' => t('Connection information'),
    '#tree' => true,
    '#type' => 'fieldset'
  ];

  $form['oidc_implicit_connection_info']['client_id'] = [
    '#default_value' => isset($connection_info['client_id']) ? $connection_info['client_id'] : null,
    '#description' => t('Client identifier valid at the provider.'),
    '#required' => true,
    '#title' => t('Client ID'),
    '#type' => 'textfield'
  ];

  $form['oidc_implicit_connection_info']['client_secret'] = [
    '#default_value' => isset($connection_info['client_secret']) ? $connection_info['client_secret'] : null,
    '#description' => t('Client secret key provided by the Authorization server. Used to validate response from the provider.'),
    '#required' => true,
    '#title' => t('Client Secret'),
    '#type' => 'textfield'
  ];

  $form['oidc_implicit_connection_info']['endpoint'] = [
    '#default_value' => isset($connection_info['endpoint']) ? $connection_info['endpoint'] : null,
    '#description' => t('Provider URL where the user will be directed for login.'),
    '#required' => true,
    '#title' => t('Authorization Endpoint'),
    '#type' => 'textfield'
  ];

  $form['oidc_implicit_login_path'] = [
    '#default_value' => variable_get('oidc_implicit_login_path', OIDC_IMPLICIT_DEFAULT_LOGIN_PATH),
    '#description' => t('Path for logging user authentication. Do not include preceding slash.'),
    '#required' => true,
    '#title' => t('Login path'),
    '#type' => 'textfield'
  ];

  $form['oidc_implicit_successful_auth_path'] = [
    '#default_value' => variable_get('oidc_implicit_successful_auth_path', OIDC_IMPLICIT_DEFAULT_SUCCESSFUL_AUTH_PATH),
    '#description' => t('Path to redirect user after successful authentication.'),
    '#required' => true,
    '#title' => t('Successful authentication path'),
    '#type' => 'textfield'
  ];

  $form['oidc_implicit_failed_auth_path'] = [
    '#default_value' => variable_get('oidc_implicit_failed_auth_path', OIDC_IMPLICIT_DEFAULT_FAILED_AUTH_PATH),
    '#description' => t('Path to redirect user upon failed authentication attempt.'),
    '#required' => true,
    '#title' => t('Failed authentication path'),
    '#type' => 'textfield'
  ];

  return system_settings_form($form);
}

/**
 * Connection information configuration submission handler.
 */
function oidc_implicit_admin_connection_form_submit($form, &$form) {
  $old_login_path = variable_get('oidc_implicit_login_path', OIDC_IMPLICIT_DEFAULT_LOGIN_PATH);
  $new_login_path = $form_state['values']['oidc_implicit_login_path'];

  // If the login path changes, trigger a menu rebuild.
  if ($new_login_path != $old_login_path) {
    variable_set('menu_rebuild_needed', true);
  }
}

/**
 * Local authentication configuration form callback.
 */
function oidc_implicit_admin_local_auth_form() {
  $local_auth = variable_get('oidc_implicit_local_auth', []);

  $form = [];

  $form['oidc_implicit_local_auth'] = [
    '#tree' => true,
    '#type' => 'container',
  ];

  $form['oidc_implicit_local_auth']['local_login'] = [
    '#default_value' => isset($local_auth['local_login']) ? $local_auth['local_login'] : false,
    '#description' => t('Setting this value will allow users to log in with local Drupal accounts.'),
    '#title' => t('Allow authentication with local Drupal Accounts'),
    '#type' => 'checkbox'
  ];

  $form['oidc_implicit_local_auth']['restrict_oidc'] = [
    '#default_value' => isset($local_auth['restrict_oidc']) ? $local_auth['restrict_oidc'] : false,
    '#description' => t('Setting this value will restrict OpenID Connected configured users from using local Drupal authentication.<br>NOTE: Not required if no user may authenticate locally.'),
    '#states' => [
      'disabled' => [
        ':input[name="oidc_implicit_local_auth[local_login]"]' => ['checked' => false]
      ]
    ],
    '#title' => t('Restrict OIDC connected accounts from logging in locally'),
    '#type' => 'checkbox'
  ];

  $roles = user_roles(true);
  $form['oidc_implicit_local_auth']['role_bypass'] = [
    '#default_value' => isset($local_auth['role_bypass']) ? $local_auth['role_bypass'] : null,
    '#description' => t('Selected roles will be able to bypass local login restrictions set by this module.'),
    '#multiple' => true,
    '#options' => $roles,
    '#title' => t('Local account login bypass by role'),
    '#type' => 'select'
  ];

  $form['oidc_implicit_local_auth']['user_bypass'] = [
    '#default_value' => isset($local_auth['user_bypass']) ? $local_auth['user_bypass'] : null,
    '#description' => t('Example: <em>1,2,3</em><br>A comma-separated list of user IDs allowed to bypass local login restrictions set by this module.'),
    '#title' => t('Local account login bypass by user'),
    '#type' => 'textfield'
  ];

  return system_settings_form($form);
}

/**
 * User information configuration form callback.
 */
function oidc_implicit_admin_user_info_form() {
  $user_attributes = variable_get('oidc_implicit_user_attributes', []);

  $form = [];

  $form['oidc_implicit_user_attributes'] = [
    '#collapsed' => false,
    '#collapsible' => false,
    '#title' => t('User identification'),
    '#tree' => true,
    '#type' => 'fieldset'
  ];

  $form['oidc_implicit_user_attributes']['username'] = [
    '#default_value' => isset($user_attributes['username']) ? $user_attributes['username'] : null,
    '#description' => t('Example: <em>preferred_username</em>'),
    '#required' => true,
    '#title' => t('Which provider attribute should be used for the accounts username'),
    '#type' => 'textfield'
  ];

  $form['oidc_implicit_user_attributes']['email'] = [
    '#default_value' => isset($user_attributes['email']) ? $user_attributes['email'] : null,
    '#description' => t('Example: <em>email</em>'),
    '#required' => true,
    '#title' => t('Which provider attribute should be used for the accounts email'),
    '#type' => 'textfield'
  ];

  return system_settings_form($form);
}
