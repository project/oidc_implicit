<?php
/**
 * @file
 * Hooks for OpenID Connect Implicit module.
 */

/**
 * Gives modules an opportunity to act on the user account prior
 * to login with access to the provider data.
 *
 * @param array $account
 *   The user being logged in.
 * @param array $payload
 *   Array of values provided from the OIDC JWT token.
 */
function hook_oidc_implicit_pre_login($account, $payload) {}

/**
 * Allows modules to modify the user account information prior
 * to registration.
 *
 * @param array &$edit
 *   Array of user values to be registered.
 * @param array $payload
 *   Array of values provided from the OIDC JWT token.
 */
function hook_oidc_implicit_pre_register(&$edit, $payload) {}