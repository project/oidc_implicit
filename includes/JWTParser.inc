<?php
/**
 * @file
 * Definition of JWT.
 */

/**
 * Defines a class to decode/retrive/verify JSON web token payloads.
 */
class JWTParser {
  /**
   * Base64 encoded header value.
   */
  protected $encoded_header;

  /**
   * Base64 encoded payload value.
   */
  protected $encoded_paylod;

  /**
   * Decoded JWT header data.
   */
  protected $header;

  /**
   * Decoded JWT payload data.
   */
  protected $payload;

  /**
   * Decoded JWT signature.
   */
  protected $signature;

  /**
   * Constructor.
   */
  public function __construct($token) {
    // Restore URL safe base64 back into regular base64.
    $token = strtr($token, '-_', '+/');

    list($header, $payload, $signature) = explode('.', $token);

    $this->encoded_header = $header;
    $this->header = json_decode(base64_decode($header), true);

    $this->encoded_payload = $payload;
    $this->payload = json_decode(base64_decode($payload), true);

    $this->signature = base64_decode($signature);
  }

  /**
   * Get the decoded header data.
   */
  public function getHeader() {
    return $this->header;
  }

  /**
   * Get the decoded payload data.
   */
  public function getPayload() {
    return $this->payload;
  }

  /**
   * Verify token signature.
   */
  public function verifySignature($secret) {
    switch ($this->header['alg']) {
      case 'HS256':
      case 'HS384':
      case 'HS512':
        $type = 'SHA' . substr($this->header['alg'], 2);
        $hash = hash_hmac('SHA256', "{$this->encoded_header}.{$this->encoded_payload}", $secret, true);
        return hash_equals($this->signature, $hash);
      break;
      // Unsupported signature type.
      default:
        return false;
      break;
    }
  }
}
