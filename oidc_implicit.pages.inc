<?php
/**
 * @file
 * Page callbacks for OpenID Connect Implicit Flow.
 */

/**
 * Page callback to redirect the user to the provider login.
 */
function oidc_implicit_login_page() {
  $connection_info = variable_get('oidc_implicit_connection_info', []);
  $login_path = variable_get('oidc_implicit_login_path', OIDC_IMPLICIT_DEFAULT_LOGIN_PATH);

  // Nothing to do without connection information.
  if (empty($connection_info)) drupal_access_denied();

  // User already logged in.
  if (user_is_logged_in()) {
    drupal_goto(variable_get('oidc_implicit_successful_auth_path', OIDC_IMPLICIT_DEFAULT_SUCCESSFUL_AUTH_PATH));
    return;
  }

  $options = [
    'external' => true,
    'query' => [
      'client_id' => $connection_info['client_id'],
      'nonce' => oidc_implicit_set_nonce(),
      'redirect_uri' => url("$login_path/authorize", ['absolute' => true]),
      'response_type' => 'id_token',
      'scope' => 'openid'
    ]
  ];

  // Clear destination parameter incase it's set.
  unset($_GET['destination']);

  // Redirect to the provider authorization endpoint.
  drupal_goto($connection_info['endpoint'], $options);
}

/**
 * Page callback where the OpenID Connect provider redirects.
 */
function oidc_implicit_redirect_page() {
  $connection_info = variable_get('oidc_implicit_connection_info', []);

  // Nothing to do without connection information.
  if (empty($connection_info)) drupal_access_denied();

  $params = drupal_get_query_parameters();

  // Reject invalid requests.
  if (!isset($params['id_token'])) drupal_access_denied();

  // Parse the provided ID token.
  $token = new JWTParser($params['id_token']);

  $payload = $token->getPayload();

  // Validate received token and authorize user.
  if (
    // Reject invalid tokens.
    !$token->verifySignature($connection_info['client_secret'])
    // Reject non-matching nonce value to avoid replay attacks.
    || (!isset($payload['nonce'])
      || !oidc_implicit_validate_nonce($payload['nonce']))
    // Reject expired tokens.
    || (!isset($payload['exp'])
      || $payload['exp'] <= time())
    // Make sure the user is authorized.
    || !oidc_implicit_authorize($payload)
  ) {
    // Notify user of failed authentication and redirect them.
    drupal_set_message(t('Authentication could not be completed due to an error.'), 'error');
    drupal_goto(variable_get('oidc_implicit_failed_auth_path', OIDC_IMPLICIT_DEFAULT_FAILED_AUTH_PATH));
    return;
  }

  // Clear out nonce value, it's no longer needed.
  oidc_implicit_unset_nonce();

  // Redirect user after successful authentication.
  drupal_goto(variable_get('oidc_implicit_successful_auth_path', OIDC_IMPLICIT_DEFAULT_SUCCESSFUL_AUTH_PATH));
}
